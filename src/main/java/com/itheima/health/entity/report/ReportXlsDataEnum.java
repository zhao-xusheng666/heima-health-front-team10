package com.itheima.health.entity.report;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * 运营数据导出 xls 数据枚举
 *
 * @author baodsh
 * @date 2022/12/04
 */
public enum ReportXlsDataEnum {

    /**
     * 日期
     */
    DATE,
    /**
     * 新增会员数
     */
    NEWMEMBERCOUNT,
    /**
     * 总会员数
     */
    ALLMEMBERCOUNT,
    /**
     * newmemberinweekcount
     */
    NEWMEMBERINWEEKCOUNT,
    /**
     * newmemberinmonthcount
     */
    NEWMEMBERINMONTHCOUNT,
    /**
     * todayordercount
     */
    TODAYORDERCOUNT,
    /**
     * weekordercount
     */
    WEEKORDERCOUNT,
    /**
     * monthordercount
     */
    MONTHORDERCOUNT,
    /**
     * todayarrivalcount
     */
    TODAYARRIVALCOUNT,
    /**
     * weekarrivalcount
     */
    WEEKARRIVALCOUNT,
    /**
     * montharrivalcount
     */
    MONTHARRIVALCOUNT;


    static class DataCellItem {
        private Integer column;
        private Integer row;

        public DataCellItem(Integer row, Integer column) {
            this.row = row;
            this.column = column;
        }
    }

    public static DataCellItem getDataCellItem(ReportXlsDataEnum rowType) {
        switch (rowType) {
            case DATE:
                return new DataCellItem(2, 5);
            case NEWMEMBERCOUNT:
                return new DataCellItem(4, 5);
            case ALLMEMBERCOUNT:
                return new DataCellItem(4, 7);
            case NEWMEMBERINWEEKCOUNT:
                return new DataCellItem(5, 5);
            case NEWMEMBERINMONTHCOUNT:
                return new DataCellItem(5, 7);
            case TODAYORDERCOUNT:
                return new DataCellItem(7, 5);
            case TODAYARRIVALCOUNT:
                return new DataCellItem(7, 7);
            case WEEKORDERCOUNT:
                return new DataCellItem(8, 5);
            case WEEKARRIVALCOUNT:
                return new DataCellItem(8, 7);
            case MONTHORDERCOUNT:
                return new DataCellItem(9, 5);
            case MONTHARRIVALCOUNT:
                return new DataCellItem(9, 7);
            default:
                break;
        }
        return new DataCellItem(0, 0);
    }


    public static void setData(XSSFWorkbook workbook, ReportXlsDataEnum type, Object data) {
        XSSFSheet sheet = workbook.getSheetAt(0);
        DataCellItem cellItem = getDataCellItem(type);
        // XSSFRow row = sheet.createRow(cellItem.row); // 预留
        XSSFRow dataRow = sheet.getRow(cellItem.row);
        setCellData(dataRow, cellItem, data);
        XSSFCell cell = dataRow.getCell(cellItem.column);
        setBackgroundColor(workbook, cell);
    }

    public static void setRowsData(XSSFWorkbook workbook, Integer row, Integer column, Object data) {
        XSSFSheet sheet = workbook.getSheetAt(0);
        DataCellItem cellItem = new DataCellItem(12 + row, 4 + column);
        XSSFRow dataRow = sheet.getRow(cellItem.row);
        setCellData(dataRow, cellItem, data);
        XSSFCell cell = dataRow.getCell(cellItem.column);
        setBackgroundColor(workbook, cell);
    }

    public static void setCellData(XSSFRow dataRow, DataCellItem cellItem, Object data) {

        Optional.ofNullable(data).ifPresent(item -> {
            if (item instanceof Number) {
                if (item instanceof Integer) {
                    dataRow.createCell(cellItem.column).setCellValue((Integer) item);
                } else {
                    dataRow.createCell(cellItem.column).setCellValue((Double) item);
                }
            } else {
                dataRow.createCell(cellItem.column).setCellValue(item.toString());
            }
        });
    }

    public static void setBackgroundColor(XSSFWorkbook workbook, XSSFCell cell) {

        XSSFCellStyle cellStyle = workbook.createCellStyle();
        XSSFColor color = new XSSFColor(new java.awt.Color(220, 230, 241));
        cellStyle.setFillForegroundColor(color);
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBottomBorderColor(HSSFColor.BLACK.index);
        cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        cellStyle.setLeftBorderColor(HSSFColor.BLACK.index);
        cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        cellStyle.setRightBorderColor(HSSFColor.BLACK.index);
        cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        cellStyle.setTopBorderColor(HSSFColor.BLACK.index);

        XSSFFont font = workbook.createFont();
        font.setFontName("宋体");
        font.setFontHeightInPoints((short) 16);// 设置字体大小
        cellStyle.setFont(font);// 选择需要用到的字体格式

        cell.setCellStyle(cellStyle);
    }

    // 工具方法
    public static <T> Consumer<T> consumerWithIndex(BiConsumer<T, Integer> consumer) {
        class Obj {
            int i;
        }
        Obj obj = new Obj();
        return t -> {
            int index = obj.i++;
            consumer.accept(t, index);
        };
    }
}
