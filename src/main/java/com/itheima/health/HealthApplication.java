package com.itheima.health;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author ：spt
 * @date ：Created in 2022/06/29
 * @description ：
 * @version: 1.0
 */
@SpringBootApplication
@EnableScheduling
@MapperScan("com.itheima.health.dao")
@ServletComponentScan
public class HealthApplication {

    public static void main(String[] args) {
        SpringApplication.run(HealthApplication.class, args);
    }

}