package com.itheima.health.service;

import com.itheima.health.pojo.OrderSetting;

import java.util.List;
import java.util.Map;

/**
 * @author spt
 * @description 预约设置SERVICE
 * @date 2019/9/29
 **/
public interface OrderSettingService {

//预约信息展示
List<OrderSetting> getOrderSettingByMonth(String year,String month );

//预约设置
    void editNumberByDate(OrderSetting orderSetting);

    int insertBatch_orderSetting(List<OrderSetting> orderSettings);

    //根据预约时间查询对应的预约配置信息
    OrderSetting selectOrderSetting(String orderDate);

    //根据ordersetting id 更新已预约人数
    void updateOrderSettingById(OrderSetting orderSetting);
}
