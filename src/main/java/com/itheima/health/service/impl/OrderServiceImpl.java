package com.itheima.health.service.impl;

import com.itheima.health.dao.OrderDao;
import com.itheima.health.dao.OrderSettingDao;
import com.itheima.health.dto.OrderDto;
import com.itheima.health.pojo.Order;
import com.itheima.health.service.OrderService;
import com.itheima.health.vo.SetmealParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 预约ServiceImpl
 */
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;
    //套餐


    @Override
    public SetmealParam selectOrerBySetmealid() {
        //查询订单中的有那些套餐id
        List<Map<String, Object>> ids = orderDao.selectbysetmatealId();
        SetmealParam setmealParam = new SetmealParam();
        List<String> list = new ArrayList<>();
        List<Map<String,Object>> list2 = new ArrayList<>();
        for (Map<String, Object> id : ids) {
            list.add(id.get("name").toString());
            list2.add(id);
        }
        setmealParam.setSetmealNames(list);
        setmealParam.setSetmealCount(list2);
        return setmealParam;
    }


    //条件查询order信息
    @Override
    public Order selectOrder(Order order) {
        return orderDao.selectOrder(order);
    }


    //添加预约信息
    @Override
    public int insertOrder(Order order) {
        return orderDao.insertOrder(order);
    }

    @Override
    public OrderDto selectOrderDtoById(Integer id) {
        OrderDto orderDto=orderDao.selectOrderDtoById(id);
        return orderDto;
    }
}
