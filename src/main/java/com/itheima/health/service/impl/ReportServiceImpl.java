package com.itheima.health.service.impl;

import com.itheima.health.dao.MemberDao;
import com.itheima.health.dao.OrderDao;
import com.itheima.health.dto.ReportExportDto;
import com.itheima.health.dto.ReportHotSetmeal;
import com.itheima.health.entity.report.ReportXlsDataEnum;
import com.itheima.health.service.ReportService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import static com.itheima.health.entity.report.ReportXlsDataEnum.consumerWithIndex;

/**
 * 数据报表service
 */
@Service
@Slf4j
public class ReportServiceImpl implements ReportService {
    @Autowired
    private MemberDao memberDao;
    @Autowired
    private OrderDao orderDao;


    /**
     * 报告导出方法
     *
     * @param outputStream    输出流
     * @param reportExportDto 报告导出dto
     * @throws Exception 异常
     */
    @Override
    public void exportWithReportModel(ServletOutputStream outputStream, ReportExportDto reportExportDto) throws Exception {

        // get Template
        ClassPathResource classPathResource = new ClassPathResource("report_template.xlsx");
        InputStream inputStream = classPathResource.getInputStream();
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

        // 设置数据
        ReportXlsDataEnum.setData(workbook, ReportXlsDataEnum.DATE, reportExportDto.getReportDate());

        ReportXlsDataEnum.setData(workbook, ReportXlsDataEnum.NEWMEMBERCOUNT,
                reportExportDto.getTodayNewMember());
        ReportXlsDataEnum.setData(workbook, ReportXlsDataEnum.ALLMEMBERCOUNT,
                reportExportDto.getTotalMember());
        ReportXlsDataEnum.setData(workbook, ReportXlsDataEnum.NEWMEMBERINWEEKCOUNT,
                reportExportDto.getThisWeekNewMember());
        ReportXlsDataEnum.setData(workbook, ReportXlsDataEnum.NEWMEMBERINMONTHCOUNT,
                reportExportDto.getThisMonthNewMember());

        ReportXlsDataEnum.setData(workbook, ReportXlsDataEnum.TODAYORDERCOUNT,
                reportExportDto.getTodayOrderNumber());
        ReportXlsDataEnum.setData(workbook, ReportXlsDataEnum.TODAYARRIVALCOUNT,
                reportExportDto.getTodayVisitsNumber());
        ReportXlsDataEnum.setData(workbook, ReportXlsDataEnum.WEEKORDERCOUNT,
                reportExportDto.getThisWeekOrderNumber());
        ReportXlsDataEnum.setData(workbook, ReportXlsDataEnum.WEEKARRIVALCOUNT,
                reportExportDto.getThisWeekVisitsNumber());
        ReportXlsDataEnum.setData(workbook, ReportXlsDataEnum.MONTHORDERCOUNT,
                reportExportDto.getThisMonthOrderNumber());
        ReportXlsDataEnum.setData(workbook, ReportXlsDataEnum.MONTHARRIVALCOUNT,
                reportExportDto.getThisMonthVisitsNumber());

        reportExportDto.getHotSetmeal().forEach(consumerWithIndex((item, index) -> {
            ReportXlsDataEnum.setRowsData(workbook, index, 0, item.getName());
            ReportXlsDataEnum.setRowsData(workbook, index, 1, item.getSetmeal_count());
            ReportXlsDataEnum.setRowsData(workbook, index, 2, item.getProportion());
            ReportXlsDataEnum.setRowsData(workbook, index, 3, item.getRemark());
        }));

        workbook.write(outputStream);
        outputStream.flush();// 清空缓冲区
        outputStream.close();
        workbook.close();

    }

    /**
     * 查询会员数量折线图数据1：会员注册时间月份
     * @return
     */
    @Override
    public String[] findMemberRegMonthsArr() {
        return memberDao.findMemberRegMonthsArr();
    }
    /**
     * 查询会员数量折线图数据2：月份对应的会员注册数量
     * @return
     */
    @Override
    public Integer[] findMemberCount() {
        return memberDao.findMemberCount();
    }

    /**
     * 查询客户统计数据：2.查询ReportHotSetmeal集合(2条)
     * @return
     */
    @Override
    public List<ReportHotSetmeal> findReportHotSetmealList() {
        return orderDao.findReportHotSetmealList();
    }

    /**
     * 查询客户统计数据：1.查询reportExportDto基础数据：
     *  日期：date
     *  本日新增会员数：newMemberCount
     *  总会员数：allMemberCount
     *  本周新增会员数：newMemberInWeekCount
     *  本月新增会员数：newMemberInMonthCount
     *
     *  今日预约数：todayOrderCount
     *  本周预约数：weekOrderCount
     *  本月预约数：monthOrderCount
     *
     *  今日到诊数：todayArrivalCount
     *  本周到诊数：weekArrivalCount
     *  本月到诊数：monthArrivalCount
     * @return
     */
    @Override
    public ReportExportDto getReportExportDto() {
        ReportExportDto reportExportDto = new ReportExportDto();
        String today = String.format("%tY" ,new Date())+"-"+String.format("%tm" ,new Date())+"-"+String.format("%td" ,new Date());
        reportExportDto.setReportDate(today);//日期：date
        reportExportDto.setTodayNewMember(memberDao.selectNewMemberCount("day"));//本日新增会员数：newMemberCount
        reportExportDto.setThisWeekNewMember(memberDao.selectNewMemberCount("yearweek"));//本周新增会员数：newMemberInWeekCount
        reportExportDto.setThisMonthNewMember(memberDao.selectNewMemberCount("month"));//本月新增会员数：newMemberInMonthCount
        reportExportDto.setTotalMember(memberDao.selectNewMemberCount(null));//总会员数：allMemberCount

        reportExportDto.setTodayOrderNumber(orderDao.selectOrderCount("day",null));//今日预约数：todayOrderCount
        reportExportDto.setThisWeekOrderNumber(orderDao.selectOrderCount("yearweek",null));//本周预约数：weekOrderCount
        reportExportDto.setThisMonthOrderNumber(orderDao.selectOrderCount("month",null));//本月到诊数：monthArrivalCount

        reportExportDto.setTodayVisitsNumber(orderDao.selectOrderCount("day","已到诊"));//今日到诊数：todayArrivalCount
        reportExportDto.setThisWeekVisitsNumber(orderDao.selectOrderCount("yearweek","已到诊"));//本周到诊数：weekArrivalCount
        reportExportDto.setThisMonthVisitsNumber(orderDao.selectOrderCount("month","已到诊"));//本月到诊数：monthArrivalCount
        return reportExportDto;
    }

}
