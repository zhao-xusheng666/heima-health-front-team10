package com.itheima.health.service.impl;

import com.itheima.health.dao.MemberDao;
import com.itheima.health.pojo.Member;
import com.itheima.health.service.MemberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 会员SERVICE实现类
 */
@Service
@Slf4j
public class MemberServiceImpl implements MemberService {
    @Autowired
    private MemberDao memberDao;

    /**
     * 根据实体类参数查询会员信息
     * @param member
     * @return
     */
    @Override
    public Member selectMember(Member member) {
        return memberDao.selectMember(member);
    }

    /**
     * 添加会员信息
     * @param member
     * @return
     */
    @Override
    public int insertMember(Member member) {
        return memberDao.insertMember(member);
    }
}
