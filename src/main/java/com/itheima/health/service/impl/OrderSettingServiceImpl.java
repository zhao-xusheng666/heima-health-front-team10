package com.itheima.health.service.impl;

import com.itheima.health.dao.OrderSettingDao;
import com.itheima.health.pojo.OrderSetting;
import com.itheima.health.service.OrderSettingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author spt
 * @description
 * @date 2019/9/29
 **/
@Service
@Slf4j
public class OrderSettingServiceImpl implements OrderSettingService {
    @Autowired
    private OrderSettingDao orderSettingDao;

    /**
     * 预约信息展示
     *
     * @param
     * @return
     */
    @Override
    @Transactional
    public List<OrderSetting> getOrderSettingByMonth(String year, String month) {
        //拼接
        String dateBegin = year + "-" + month + "-01"; //2022-1-1
        String dateEnd = year + "-" + month + "-31"; //2022-1-31
        //定义一个map 将数据添加进去
        HashMap<String, String> map = new HashMap<>();
        map.put("dateBegin", dateBegin);
        map.put("dateEnd", dateEnd);
//调用方法，用list接收
        List<OrderSetting> list = orderSettingDao.getOrderSettingByMonth(dateBegin, dateEnd);
        return list;
    }

    /**
     * 预约设置
     *
     * @param orderSetting
     */
    @Override
    public void editNumberByDate(OrderSetting orderSetting) {
        //如果预约设置已经设置过，更新
        System.out.println(orderSetting.getOrderDate());
        int count =  orderSettingDao.getCountByOrderDate(orderSetting);
        if (count > 0) {
            orderSettingDao.saveMyOrderSetting(orderSetting);
        }else {
            //如果没有设置过，直接插入新数据
            orderSettingDao.insertNewOrderSetting(orderSetting);
        }
    }
    @Override
    public int insertBatch_orderSetting(List<OrderSetting> orderSettings) {
        int count=0;
        for (OrderSetting orderSetting : orderSettings) {
            count+=orderSettingDao.insert_orderSetting(orderSetting);
        }
       return count;
    }

    @Override
    public OrderSetting selectOrderSetting(String orderDate) {
        return orderSettingDao.selectOrderSetting(orderDate);
    }

    @Override
    public void updateOrderSettingById(OrderSetting orderSetting) {
        orderSettingDao.updateOrderSettingById(orderSetting);
    }
}
