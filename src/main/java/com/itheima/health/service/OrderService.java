package com.itheima.health.service;


import com.itheima.health.dto.OrderDto;
import com.itheima.health.pojo.Order;
import com.itheima.health.vo.SetmealParam;



/**
 * 预约Service
 */
public interface OrderService {

    /***
     * 返回订单数据中套餐的id 以及个数
     * @return
     */
    SetmealParam selectOrerBySetmealid();
    //条件查询order信息
    Order selectOrder(Order order);


    //添加预约信息
    int insertOrder(Order order);

    OrderDto selectOrderDtoById(Integer id);
}
