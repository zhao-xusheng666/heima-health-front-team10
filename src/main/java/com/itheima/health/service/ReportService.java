package com.itheima.health.service;

import com.itheima.health.dto.ReportExportDto;
import com.itheima.health.dto.ReportHotSetmeal;

import javax.servlet.ServletOutputStream;
import java.util.List;

/**
 * 数据报表SERVICE
 */
public interface ReportService {


    public void exportWithReportModel(ServletOutputStream outputStream, ReportExportDto reportExportDto) throws Exception;

    String[] findMemberRegMonthsArr();

    Integer[] findMemberCount();

    List<ReportHotSetmeal> findReportHotSetmealList();

    ReportExportDto getReportExportDto();
}
