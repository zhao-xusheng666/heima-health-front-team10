package com.itheima.health.service;

import com.itheima.health.pojo.Member;

import java.util.List;


/**
 * 会员Service
 */
public interface MemberService {
    //根据实体类参数查询会员信息
    Member selectMember(Member member);

    /**
     * 添加会员信息
     * @param member
     * @return
     */
    int insertMember(Member member);
}
