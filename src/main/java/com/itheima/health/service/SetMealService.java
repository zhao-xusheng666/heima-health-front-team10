package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.Setmeal;

import java.util.List;
import java.util.Map;

/**
 * @author spt
 * @description 套餐SERVICE
 * @date 2019/9/26
 **/
public interface SetMealService {

    /**
     * 添加套餐
     * @param setmeal
     * @param checkgroupIds
     */
    void add(Setmeal setmeal, Integer[] checkgroupIds);

    /**
     * 分页查询
     * @param queryPageBean
     * @return
     */
    PageResult findPage(QueryPageBean queryPageBean);

    //根据id查询套餐
    Map<String,Object> selectSetMealFindById(Integer id);

    /**
     * 手机端套餐查询
     * @param setmeal
     * @return
     */
    List<Setmeal> slete(Setmeal setmeal);
}
