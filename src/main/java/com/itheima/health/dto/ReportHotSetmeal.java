package com.itheima.health.dto;

import lombok.Data;

/**
 *
 * ClassName ReportHotSetmeals
 * Description
 * Date 2022/12/4 18:14
 *
 * @author baodsh
 * @version 1.0
 * @date 2022/12/05
 */
@Data
public class ReportHotSetmeal {
    /**
     * 套餐名称
     */
    private String name;
    /**
     * 预约数量
     */
    private Integer setmeal_count;
    /**
     * 占比
     */
    private Double proportion;
    /**
     * 备注
     */
    private String remark;
}