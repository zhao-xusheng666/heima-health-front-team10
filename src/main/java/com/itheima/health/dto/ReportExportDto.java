package com.itheima.health.dto;

import lombok.Data;

import java.util.List;

/**
 * ClassName ReportExportDto
 * Description
 * Date 2022/12/4 18:08
 *
 * @author baodsh
 * @version 1.0
 **/
@Data
public class ReportExportDto {

    /**
     * 日期
     */
    private String reportDate;
    /**
     * 新增会员数
     */
    private Integer todayNewMember;
    /**
     * 总会员数
     */
    private Integer totalMember;
    /**
     * 本周新增会员数
     */
    private Integer thisWeekNewMember;
    /**
     * 本月新增会员数
     */
    private Integer thisMonthNewMember;

    /**
     * 今日预约数
     */
    private Integer todayOrderNumber;

    /**
     * 本周预约数
     */
    private Integer thisWeekOrderNumber;

    /**
     * 本月预约数
     */
    private Integer thisMonthOrderNumber;

    /**
     * 今日到诊数
     */
    private Integer todayVisitsNumber;

    /**
     * 本周到诊数
     */
    private Integer thisWeekVisitsNumber;

    /**
     * 本月到诊数
     */
    private Integer thisMonthVisitsNumber;


    /**
     * 热门套餐
     */
    private List<ReportHotSetmeal> hotSetmeal;
}
