package com.itheima.health.dto;

import com.itheima.health.pojo.Order;
import lombok.Data;

import java.io.Serializable;

@Data
public class OrderDto extends Order implements Serializable {
    private String member;//体检人
    private String setmeal;//体检套餐

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderDto orderDto = (OrderDto) o;

        if (member != null ? !member.equals(orderDto.member) : orderDto.member != null) return false;
        return setmeal != null ? setmeal.equals(orderDto.setmeal) : orderDto.setmeal == null;
    }

    @Override
    public int hashCode() {
        int result = member != null ? member.hashCode() : 0;
        result = 31 * result + (setmeal != null ? setmeal.hashCode() : 0);
        return result;
    }
}
