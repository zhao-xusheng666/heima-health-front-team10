package com.itheima.health.vo;

import lombok.Data;

import java.util.List;
import java.util.Map;


/**
 * 套餐占比封装类
 */
@Data
public class SetmealParam {
    private List<String> setmealNames;
    private List<Map<String,Object>> setmealCount;
}
