package com.itheima.health.dao;

import com.itheima.health.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author ：spt
 * @description ：用户DAO
 * @version: 1.0
 */
@Mapper
public interface UserDao {

    /**
     * 根据userName查询
     * @param username
     * @return
     */
    User selectByUsername(@Param("username") String username);
}
