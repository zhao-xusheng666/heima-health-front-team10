package com.itheima.health.dao;

import com.itheima.health.dto.OrderDto;
import com.itheima.health.dto.ReportHotSetmeal;
import com.itheima.health.pojo.Order;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 预约DAO
 */
@Mapper
public interface OrderDao {
    /***
     * 查找订单数据中套餐的id值数组
     * @return
     */

    List<Map<String, Object>> selectbysetmatealId();


    List<ReportHotSetmeal> findReportHotSetmealList();


    Integer selectOrderCount(@Param("dateType") String dateType, @Param("orderStatus")String orderStatus);

    //条件查询order信息
    Order selectOrder(Order order);

    //添加预约信息
    int insertOrder(Order order);

    OrderDto selectOrderDtoById(@Param("id") Integer id);
}
