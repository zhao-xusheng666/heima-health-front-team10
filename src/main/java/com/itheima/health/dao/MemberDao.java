package com.itheima.health.dao;

import com.itheima.health.pojo.Member;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;

/**
 * 会员DAO
 */
public interface MemberDao {

    String[] findMemberRegMonthsArr();

    Integer[] findMemberCount();

    Integer selectNewMemberCount(@Param("dateType") String dateType);

    //根据实体类参数查询会员信息
    Member selectMember(@Param("member") Member member);

    /**
     * 添加会员信息
     * @param member
     * @return
     */
    int insertMember(@Param("member") Member member);
}
