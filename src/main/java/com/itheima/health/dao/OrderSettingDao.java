package com.itheima.health.dao;

import com.itheima.health.pojo.OrderSetting;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author spt
 * @description 预约设置DAO
 * @date 2019/9/29
 **/
@Mapper
public interface OrderSettingDao {

//根据月份查询预约设置信息
    List<OrderSetting> getOrderSettingByMonth(@Param("dateBegin") String dateBegin,@Param("dateEnd")String dateEnd);
    //根据日期找当天预约设置数据
    int getCountByOrderDate(OrderSetting orderSetting);
    //    更新预约设置
    void saveMyOrderSetting(OrderSetting orderSetting);
//保存预约设置信息
    void insertNewOrderSetting( OrderSetting orderSetting);


    int insert_orderSetting(@Param("orderSetting") OrderSetting orderSetting);

    //根据预约时间查询对应的预约配置信息
    OrderSetting selectOrderSetting(@Param("orderDate")String orderDate);

    //根据ordersetting id 更新已预约人数
    void updateOrderSettingById(@Param("orderSetting") OrderSetting orderSetting);
}
