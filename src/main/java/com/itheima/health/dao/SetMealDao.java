package com.itheima.health.dao;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.Setmeal;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * @author spt
 * @description 套餐DAO
 **/
@Mapper
public interface SetMealDao {
    /**
     * 插入
     * @param setmeal
     */
    void insert(Setmeal setmeal);

    /**
     * 插入与检查组关联关系
     * @param setmealId
     * @param checkgroupId
     */
    void insertSetMealAndCheckGroup(@Param("setmealId") Integer setmealId, @Param("checkgroupId") Integer checkgroupId);

    /**
     * 根据条件分页查询
     * @param queryString
     * @return
     */
    Page<Setmeal> selectByCondition(@Param("queryString") String queryString);

    //根据id查询套餐
    Map<String,Object> selectSetMealFindById(@Param("id") Integer id);

    List<Setmeal> selete(Setmeal setmeal);
}
