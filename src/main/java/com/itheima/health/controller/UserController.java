package com.itheima.health.controller;

import com.itheima.health.common.MessageConst;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.User;
import com.itheima.health.service.UserService;
import com.itheima.health.util.PasswordUtils;
import com.itheima.health.vo.LoginParam;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author spt
 * @description
 **/
@RestController
@RequestMapping("/user")
@Slf4j
@Api(tags = "用户接口")
public class UserController {
    private static final String CURRENT_USER = "CURRENT_USER";
    @Autowired
    private UserService userService;

    // 新增注释
    // 新增注释3 修改4444

    /**
     * 根据用户名和密码登录
     *
     * @param request
     * @return
     */
    @PostMapping("/login")
    public Result login(HttpServletRequest request, @RequestBody LoginParam param) {
        log.info("[登录]data:{}",param);
        //rpc调用查询用户信息
        User user = userService.findByUsername(param.getUsername());
        //用户不存在或密码不匹配则登录失败
        //String password = DigestUtils.md5DigestAsHex(param.getPassword().getBytes());
//        String password = param.getPassword();
        //修改验证方式
        boolean b = PasswordUtils.checkPassword(param.getPassword(), user.getPassword());
        if (!b) {
            log.info("[登录]失败，user:{}",param.getUsername());

            return new Result(false, MessageConst.LOGIN_FAIL);
        }
        //模拟用户登录成功
        log.info("[登录]成功，user:{}",user.getUsername());
        request.getSession().setAttribute("user",user);
        return new Result(true, MessageConst.LOGIN_SUCCESS);
    }
    @GetMapping("/logout")
    public Result logout(HttpServletRequest request){
        request.getSession().invalidate();
        return new Result(true, MessageConst.ACTION_SUCCESS);
    }
}
