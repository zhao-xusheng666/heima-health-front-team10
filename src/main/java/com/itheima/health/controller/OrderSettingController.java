package com.itheima.health.controller;

import com.itheima.health.common.MessageConst;
import com.itheima.health.entity.Result;
import com.itheima.health.exception.BusinessRuntimeException;
import com.itheima.health.pojo.OrderSetting;
import com.itheima.health.service.OrderSettingService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author spt
 * @description 预约设置控制器
 * @date
 **/
@RestController
@RequestMapping("/ordersetting")
@Slf4j
@Api(tags = "后台预约设置接口")
public class OrderSettingController {
    @Autowired
    OrderSettingService orderSettingService;

    @PostMapping("/upload")
    public Result upload(@RequestBody MultipartFile excelFile) throws IOException {
        InputStream in = excelFile.getInputStream();
        XSSFWorkbook workbook = new XSSFWorkbook(in);
        XSSFSheet sheet = workbook.getSheetAt(0);
        int i = 0;
        List<OrderSetting> orderSettings = new ArrayList<>();

        for (Row cells : sheet) {
            if (i > 0) {
                Date orderDate = null;
                Integer number = null;
                for (Cell cell : cells) {
                    if (cell.getColumnIndex() == 0) {
                        orderDate = cell.getDateCellValue();
                    }
                    if (cell.getColumnIndex() == 1) {
                        number = (int) cell.getNumericCellValue();
                    }
                }
                OrderSetting orderSetting = new OrderSetting(orderDate, number);
                orderSettings.add(orderSetting);
            }
            i++;
        }

        int count = orderSettingService.insertBatch_orderSetting(orderSettings);
        return new Result(count > 0, MessageConst.ORDERSETTING_SUCCESS);
    }



    /**
     * 预约信息展示
     * @param
     * @return
     */
    @GetMapping("/getOrderSettingByMonth")
    public Result getOrderSettingByMonth(String year,String month) {

        List<OrderSetting> orderSettingByMonth = orderSettingService.getOrderSettingByMonth(year, month);
//        定义一个集合
        List<Map<String, Object>> map = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();

        for (OrderSetting orderSetting : orderSettingByMonth) {
            HashMap<String, Object> hashMap = new HashMap<>();
            calendar.setTime(orderSetting.getOrderDate());
            hashMap.put("date",calendar.get(Calendar.DATE));
            hashMap.put("number",orderSetting.getNumber());
            hashMap.put("reservations",orderSetting.getReservations());
            map.add(hashMap);
        }

        return new Result(true, MessageConst.GET_ORDERSETTING_SUCCESS,map);

    }

    /**
     * 预约设置
     * @param orderSetting
     * @return
     */
    @PostMapping("/editNumberByDate")
    public Result editNumberByDate(@RequestBody OrderSetting orderSetting) {
        orderSettingService.editNumberByDate(orderSetting);
        return new Result(true,MessageConst.ORDERSETTING_SUCCESS);
    }

}
