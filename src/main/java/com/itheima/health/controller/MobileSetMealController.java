package com.itheima.health.controller;

import com.itheima.health.common.MessageConst;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Setmeal;
import com.itheima.health.service.CheckGroupService;
import com.itheima.health.service.CheckItemService;
import com.itheima.health.service.SetMealService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：石破天
 * @date ：Created in 2022年06月07日
 * @description ：
 * @version: 1.0
 */
@RestController
@RequestMapping("/mobile/setmeal")
@Slf4j
@Api(tags = "手机端套餐接口")
public class MobileSetMealController {

    @Autowired
    private SetMealService setMealService;

    @Autowired
    private CheckGroupService checkGroupService;

    @Autowired
    private CheckItemService checkItemService;

    //任务9 体检预约功能-套餐详情
    @GetMapping("/findById")
    public Result findById(Integer id){
        log.info("接收过来的套餐id"+id);
        //查询套餐
        Map<String,Object> setmeal = setMealService.selectSetMealFindById(id);
        //查询检查组
        //为什么要用List Map接收，因为套餐里面包含了多条检查组，并且检查组中还需要包含对应的检查项，所以不用实体类接收
        List<Map<String,Object>> checkGroups= checkGroupService.selectCheckGroupBySetmealId(id);
        //因为查询出的检查组是多条而且需要给每个检查组查出包含的检查项
        for (Map<String, Object> checkGroup : checkGroups) {
            //取出检查组的id，方便后面根据检查组ID查询
            String checkGroupId = checkGroup.get("id")+"";
            //根据检查组id所包含的所有检查项
            List<Map<String,Object>> checkItems = checkItemService.selectCheckItemByCheckGroupId(Integer.parseInt(checkGroupId));
            //将查出来的检查项赋予给当前的检查组
            checkGroup.put("checkItems",checkItems);
        }
        //上方已经将检查组对应的检查项添加完成，在将完整的检查组信息添加到套餐里面返回给前台
        setmeal.put("checkGroups",checkGroups);
        return new Result(true,MessageConst.QUERY_SETMEAL_SUCCESS,setmeal);
    }

    /**
     手机端套餐查询
     * @param setmeal
     * @return
     */
    @GetMapping("/getSetmeal")
    public Result getSetmeal(Setmeal setmeal){
       List<Setmeal> setmeal1 = setMealService.slete(setmeal);
       return new Result(true,MessageConst.GET_SETMEAL_LIST_SUCCESS,setmeal1);
    }
}
