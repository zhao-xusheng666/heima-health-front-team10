package com.itheima.health.controller;

import com.itheima.health.dto.ReportExportDto;
import com.itheima.health.dto.ReportHotSetmeal;
import com.itheima.health.service.ReportService;
import com.itheima.health.common.MessageConst;
import com.itheima.health.entity.Result;
import com.itheima.health.service.OrderService;
import com.itheima.health.service.SetMealService;
import com.itheima.health.vo.SetmealParam;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.*;

/**
 * 数据报告控制器
 */
@RestController
@RequestMapping("/report")
@Slf4j
@Api(tags = "数据报告展示接口")
public class ReportController {


    @Autowired
    private OrderService orderService;
    @Autowired
    private SetMealService setMealService;

    @Autowired
    private ReportService reportService;

    /**
     * 统计分析-套餐预约占比饼形图
     *
     * @return
     */
    @GetMapping("/getSetmealReport")
    public Result getSetmealReport() {


        /**
         * 订单中体检套餐的id 以及id 的个数
         */
        SetmealParam ids = orderService.selectOrerBySetmealid();


        return new Result(true,MessageConst.GET_BUSINESS_REPORT_SUCCESS,ids);
    }


    @GetMapping("/test")
    public void downloadReport(HttpServletResponse response) throws Exception {

        ReportExportDto reportExportDto = new ReportExportDto();
        reportExportDto.setReportDate("123");
        reportExportDto.setTodayNewMember(1);
        reportExportDto.setTotalMember(2);
        reportExportDto.setThisWeekNewMember(3);
        reportExportDto.setThisMonthNewMember(4);

        reportExportDto.setTodayOrderNumber(5);
        reportExportDto.setTodayVisitsNumber(6);
        reportExportDto.setThisWeekOrderNumber(7);
        reportExportDto.setThisWeekVisitsNumber(8);
        reportExportDto.setThisMonthOrderNumber(9);
        reportExportDto.setThisMonthVisitsNumber(10);

        ReportHotSetmeal setmeal1 = new ReportHotSetmeal();
        setmeal1.setName("套餐1");
        setmeal1.setRemark("套餐1备注");
        setmeal1.setSetmeal_count(111);
        setmeal1.setProportion(70.91D);
        List<ReportHotSetmeal> list = new ArrayList<>();
        list.add(setmeal1);
        reportExportDto.setHotSetmeal(list);

        response.setContentType("application/octet-stream");
        response.addHeader("Content-Disposition",
                "attachment; filename=" + URLEncoder.encode("运营数据统计.xlsx", "UTF-8"));

        reportService.exportWithReportModel(response.getOutputStream(),reportExportDto);
    }

    /**
     * 查询会员数量折线图数据
     * @return
     */
    @GetMapping("/getMemberReport")
    public Result getMemberReport(){
        Map<String, Object> map = new HashMap<>();
        String[] months = reportService.findMemberRegMonthsArr();
        Integer[] memberCount = reportService.findMemberCount();
        map.put("months", months);
        map.put("memberCount", memberCount);
        return new Result(months!=null&&memberCount!=null, MessageConst.GET_MEMBER_NUMBER_REPORT_SUCCESS, map);
    }

    /**
     * 查询客户统计数据
     * 1.查询reportExportDto所有数据并注入
     * 2.查询ReportHotSetmeal集合并注入(2条)
     * @return
     */
    @GetMapping("/getBusinessReportData")
    public Result getBusinessReportData(){
        ReportExportDto reportExportDto = reportService.getReportExportDto();
        List<ReportHotSetmeal> reportHotSetmealList = reportService.findReportHotSetmealList();

        reportExportDto.setHotSetmeal(reportHotSetmealList);
        return new Result(reportExportDto!=null, MessageConst.GET_ORDERSETTING_SUCCESS,reportExportDto);
    }
}
