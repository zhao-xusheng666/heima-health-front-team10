package com.itheima.health.controller;


import com.itheima.health.common.MessageConst;
import com.itheima.health.common.RedisConst;
import com.itheima.health.dto.OrderDto;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Member;
import com.itheima.health.pojo.Order;
import com.itheima.health.pojo.OrderSetting;
import com.itheima.health.service.MemberService;
import com.itheima.health.service.OrderService;
import com.itheima.health.service.OrderSettingService;
import com.itheima.health.service.SetMealService;
import com.itheima.health.vo.OrderSubmitParam;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * 预约控制器
 */
@RestController
@RequestMapping("/mobile/order")
@Slf4j
@Api(tags = "手机端订单接口")
public class MobileOrderController {
    @Autowired
    private MemberService memberService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderSettingService orderSettingService;

    @PostMapping("/submit")
    public Result add(@RequestBody Map<String, Object> parameter) throws Exception {
        //System.out.println(parameter.get("validateCode"));
        //判断前端用户的手机号和验证码不能为空
        if (StringUtils.isEmpty(parameter.get("validateCode")) || StringUtils.isEmpty(parameter.get("telephone"))) {
            return new Result(false, "验证码或手机号不能为空！");
        }
        if (StringUtils.isEmpty(parameter.get("name")) || StringUtils.isEmpty(parameter.get("idCard"))) {
            return new Result(false, "姓名或身份证号不能为空！");
        }
        //判断用户的验证码和已经发送存在redis中的验证码是否一致
        String telephone = (String) parameter.get("telephone");
        String validateCode = (String) parameter.get("validateCode");
        String code = (String) redisTemplate.opsForValue().get(RedisConst.VALIDATE_CODE_PREFIX + "_ORDER_" + telephone);
        if (code==null||!code.equals(validateCode)) {
            return new Result(false, "验证码错误！");
        }
        //判断预约时间
        if (StringUtils.isEmpty(parameter.get("orderDate"))) {
            return new Result(false, "请设置预约时间");
        }

        //判断用户是否注册过，如果没有注册先去注册
        Member resultMember = null;
        Member member = new Member();
        member.setPhoneNumber(telephone);
        resultMember = memberService.selectMember(member);
        //说明这个手机号没有注册,给他赋值注册
        if (resultMember == null) {
            member.setIdCard((String) parameter.get("idCard"));
            member.setName((String) parameter.get("name"));
            member.setSex((String) parameter.get("sex"));
            int count = memberService.insertMember(member);
            resultMember = memberService.selectMember(member);
        }
        //说明是老会员，判断是不是预约过，用当前传过来的时间判断
        Order order = new Order();
        order.setMemberId(resultMember.getId());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        order.setOrderDate(simpleDateFormat.parse((String) parameter.get("orderDate")));
        //查询预约表信息
        Order resultOrder = orderService.selectOrder(order);
        //如果不为空说明已经预约过了，不能重复预约
        if (resultOrder != null) {
            return new Result(false, "体检日期已预约，不可重复预约！");
        }
        //判断体检时间是不是预约满了，如果满了也不能预约
        OrderSetting orderSetting = orderSettingService.selectOrderSetting((String) parameter.get("orderDate"));
        //为什么要判断为空，看看后台有没有设置此体检时间的配置信息可预约人数
        if (orderSetting == null) {
            return new Result(false, "此体检时间未查询到可预约人数！");
        } else {
            if (orderSetting.getNumber() - orderSetting.getReservations() <= 0) {
                //说明预约人数已满不可预约
                return new Result(false, MessageConst.ORDER_FULL);
            }
        }
        //可以执行预约
        order.setOrderType(Order.ORDERTYPE_WEIXIN);//微信预约
        order.setOrderStatus(Order.ORDERSTATUS_NO);//未到诊
        String setmealId = (String) parameter.get("setmealId");
        order.setSetmealId(Integer.parseInt(setmealId));
        //执行添加预约信息
        int count = orderService.insertOrder(order);

        //预约成功后需要给预约信息配置表已预约人数加1
        orderSetting.setReservations(orderSetting.getReservations()+1);
        orderSettingService.updateOrderSettingById(orderSetting);
        return new Result(true, MessageConst.ORDER_SUCCESS,order);
    }
    @GetMapping("/findById")
    public Result findById(Integer id){
        OrderDto orderDto=orderService.selectOrderDtoById(id);
        return new Result(orderDto!=null,MessageConst.QUERY_ORDER_SUCCESS,orderDto);
    }
}
