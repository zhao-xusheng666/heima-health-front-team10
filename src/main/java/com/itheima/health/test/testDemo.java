package com.itheima.health.test;

import org.junit.jupiter.api.Test;

import java.util.Date;

public class testDemo {

    @Test
    public void testStringFormat() {
        Date myDate = new Date();
        System.out.println("当前时间："+myDate);

        int year = Integer.parseInt(String.format("%tY",myDate));
        System.out.println("年："+year);
        int month = Integer.parseInt(String.format("%tm",myDate));
        System.out.println("月："+month);
        int day = Integer.parseInt(String.format("%td",myDate));
        System.out.println("日："+day);
        int hour = Integer.parseInt(String.format("%tH",myDate));
        System.out.println("时："+hour);
        int minute = Integer.parseInt(String.format("%tM",myDate));
        System.out.println("分："+minute);
        int second = Integer.parseInt(String.format("%tS",myDate));
        System.out.println("秒："+second);
        int dayLength = Integer.parseInt(String.format("%tj", myDate));
        System.out.println(dayLength);
    }
}
